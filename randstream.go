package rwmock

import (
	"io"
	"math/rand"
	"time"
)

// RandStream generates random byte stream which can be limited in size, fragmented/defragmented and shaped.
// Implements io.Reader interface.
type RandStream struct {
	size   int
	red    int
	shaper *Shaper
}

// NewRandStream creates new RandStream object.
// size - stream size. 0 unlimited length stream.
//
// minPacket and maxPacket sets range of random values that will be used for delaying reads.
// minPacket - minimum packet size (may not be satisfied for the last packet)
// maxPacket - maximum packet size
//
// minDelay and maxDelay sets range of random values that will be used for delaying reads.
// minDelay - minimum delay between packets (accuracy is ~1ms)
// maxDelay - maximum delay between packets (accuracy is ~1ms)
func NewRandStream(size, minPacket, maxPacket int, minDelay, maxDelay time.Duration) *RandStream {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	return &RandStream{
		size:   size,
		shaper: NewShaperRand(r, minPacket, maxPacket, minDelay, maxDelay),
	}
}

// Read implements io.Reader
func (rs *RandStream) Read(b []byte) (n int, err error) {
	n, err = rs.shaper.Read(b)
	if rs.size > 0 {
		if rs.size > rs.red {
			rs.red += n
			if rs.size < rs.red {
				n -= rs.red - rs.size
			}
		}
		if rs.size < rs.red {
			err = io.EOF
		}
	}

	return
}
