package rwmock

import (
	"math/rand"
	"sync/atomic"
	"time"
)

// Consumer mimics io.Writer by pretending to read at specified rate.
// Read rate is controlled by delaying Write() operations.
type Consumer struct {
	minDelay  int
	maxDelay  int
	lastWrite time.Time
	wCnt      int64
	wBytes    int64
}

// NewConsumer creates new consumer object. With the random write delay.
//
// minDelay and maxDelay sets range of random values that will be used for delaying writes.
// minDelay - minimum delay between packets (accuracy is ~1ms)
// maxDelay - maximum delay between packets (accuracy is ~1ms)
//
// NewConsumer panics if minPacket < 1 or minPacket > maxPacket or minDelay > maxDelay
func NewConsumer(minDelay, maxDelay time.Duration) *Consumer {
	if minDelay > maxDelay {
		panic("minDelay must be <= maxDelay")
	}

	return &Consumer{
		minDelay: int(minDelay),
		maxDelay: int(maxDelay),
	}
}

// Write is delaying writes.
func (c *Consumer) Write(b []byte) (int, error) {
	// Delay
	delay := int64(c.maxDelay) - time.Since(c.lastWrite).Nanoseconds()
	if c.minDelay < c.maxDelay {
		delay = rand.Int63n(int64(c.maxDelay-c.minDelay)) + int64(c.minDelay) - time.Since(c.lastWrite).Nanoseconds()
	}
	if delay > 0 {
		delay -= int64(time.Microsecond * 100) // Naive time.Sleep overhead compensation
	}
	time.Sleep(time.Duration(delay))
	atomic.AddInt64(&c.wBytes, int64(len(b)))
	atomic.AddInt64(&c.wCnt, 1)
	c.lastWrite = time.Now()
	return len(b), nil
}

// WBytes returns written byte count
func (c *Consumer) WBytes() int64 {
	return c.wBytes
}

// WCnt returns write count
func (c *Consumer) WCnt() int64 {
	return c.wCnt
}
