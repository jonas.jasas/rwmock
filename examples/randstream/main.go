package main

import (
	"gitlab.com/jonas.jasas/rwmock"
	"log"
	"time"
)

// Printing slow random byte stream generated by rwmock.RandStream
func main() {
	rs := rwmock.NewRandStream(100, 1, 30, time.Second, time.Second*3)

	log.Print("Start receiving bytes from slow stream")
	for b, n, err := make([]byte, 50), 0, error(nil); err == nil; {
		n, err = rs.Read(b)
		log.Print(b[:n])
	}
}
