package main

import (
	"fmt"
	"gitlab.com/jonas.jasas/rwmock"
	"log"
	"net/http"
	"time"
)

// Printing to STDOUT HTTP response body slowing down read rate with the rwmock.Shaper
func main() {
	if res, err := http.DefaultClient.Get("http://example.com/"); err == nil && res.StatusCode == 200 {
		sr := rwmock.NewShaperRand(res.Body, 1, 50, time.Millisecond, time.Second)
		log.Print("Mocking extremely slow network")
		for b, n, err := make([]byte, 50), 0, error(nil); err == nil; {
			n, err = sr.Read(b)
			fmt.Print(string(b[:n]))
		}
	} else {
		log.Print("Unsuccessful request")
	}
}
