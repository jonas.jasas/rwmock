package main

import (
	"gitlab.com/jonas.jasas/rwmock"
	"io"
	"log"
	"math/rand"
	"time"
)

// Example illustrates fast stream consumption by the slow io.Writer (rwmock.Consumer)
func main() {
	r := rand.New(rand.NewSource(666))
	lr := &io.LimitedReader{r, 100000}

	cons := rwmock.NewConsumer(time.Second, time.Second*3) // Consumer will accept writes every 1-3 seconds

	log.Print("Feeding stream to the slow consumer")
	io.Copy(cons, lr)
	log.Print("done.")
}
