package rwmock

import (
	"bytes"
	"io"
	"math/rand"
	"testing"
	"time"
)

func TestReadFixed(t *testing.T) {
	// Input buffer
	ib := make([]byte, 10000)
	rand.Read(ib)

	for s := 1; s < 100; s++ {
		ibr := bytes.NewBuffer(ib)
		shaper := NewShaper(ibr, s, 0)

		// Output buffer
		obr := &bytes.Buffer{}

		if _, err := io.Copy(obr, shaper); err != nil {
			t.Error(err)
		}

		if bytes.Compare(ib, obr.Bytes()) != 0 {
			t.Error("Data loss")
		}
	}

	for d := time.Duration(0); d < time.Millisecond*10; d += time.Millisecond {
		ibr := bytes.NewBuffer(ib)
		shaper := NewShaper(ibr, 100, d)

		// Output buffer
		obr := &bytes.Buffer{}

		if _, err := io.Copy(obr, shaper); err != nil {
			t.Error(err)
		}

		if bytes.Compare(ib, obr.Bytes()) != 0 {
			t.Error("Data loss")
		}
	}

}

func TestReadRndSizeDelay(t *testing.T) {
	// Input buffer
	ib := make([]byte, 100000)
	rand.Read(ib)
	ibr := bytes.NewBuffer(ib)

	shaper := NewShaperRand(ibr, 1, 1000, 0, time.Millisecond*10)

	// Output buffer
	obr := &bytes.Buffer{}

	if _, err := io.Copy(obr, shaper); err != nil {
		t.Error(err)
	}

	if bytes.Compare(ib, obr.Bytes()) != 0 {
		t.Error("Data loss")
	}
}

func TestReadTiming(t *testing.T) {
	// Input buffer
	ib := make([]byte, 1000000)
	rand.Read(ib)
	ibr := bytes.NewBuffer(ib)

	shaper := NewShaperRand(ibr, 1000, 1000, time.Millisecond, time.Millisecond)

	// Output buffer
	obr := &bytes.Buffer{}

	start := time.Now()
	if _, err := io.Copy(obr, shaper); err != nil {
		t.Error(err)
	}
	elapsed := time.Since(start)
	if elapsed < time.Millisecond*800 || elapsed > time.Millisecond*1200 {
		t.Error("Delay too inaccurate")
	}
}

func TestRndStrem(t *testing.T) {
	resBuf := &bytes.Buffer{}

	const streamSize = 1000000
	rndStream := NewRandStream(streamSize, 1, 1000, 0, time.Millisecond)
	io.Copy(resBuf, rndStream)

	if len(resBuf.Bytes()) != 1000000 {
		t.Error("Stream size mismatch")
	}
}

func TestRndStremLast(t *testing.T) {
	rndStream := NewRandStream(10000, 1, 100, 0, time.Millisecond)
	b := make([]byte, 50)
	for {
		n, err := rndStream.Read(b)
		if n == 0 && err == nil {
			t.Error("Red 0 bytes without an error")
		}
		if err != nil {
			break
		}
	}
}

func TestConsumerTiming(t *testing.T) {
	r := rand.New(rand.NewSource(666))
	lr := &io.LimitedReader{r, 1000000}
	cons := NewConsumer(0, time.Millisecond)

	start := time.Now()
	io.CopyBuffer(cons, lr, make([]byte, 1000))
	elapsed := time.Since(start)
	if elapsed < time.Millisecond*400 || elapsed > time.Millisecond*600 {
		t.Error("Delay too inaccurate")
	}

	if cons.WBytes() != 1000000 {
		t.Error("Incorrect written bytes count")
	}
	if cons.WCnt() == 0 {
		t.Error("Incorrect writes count")
	}
}

func checkPanic(t *testing.T) {
	if r := recover(); r == nil {
		t.Error("No argument validation")
	}
}

func TestBadArgs(t *testing.T) {
	defer checkPanic(t)
	NewConsumer(time.Second, time.Millisecond)
}

func TestShaperBadArgs1(t *testing.T) {
	defer checkPanic(t)
	NewShaper(&bytes.Buffer{}, 0, 0)
}

func TestShaperBadArgs2(t *testing.T) {
	defer checkPanic(t)
	NewShaperRand(&bytes.Buffer{}, 0, 100, time.Millisecond, time.Second)
}

func TestShaperBadArgs3(t *testing.T) {
	defer checkPanic(t)
	NewShaperRand(&bytes.Buffer{}, 100, 1, time.Millisecond, time.Second)
}

func TestShaperBadArgs4(t *testing.T) {
	defer checkPanic(t)
	NewShaperRand(&bytes.Buffer{}, 1, 100, time.Second, time.Millisecond)
}
