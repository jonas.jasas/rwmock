# RWMock slow producer and consumer mock-up for testing

RWMock is a Golang package that mocks slow byte stream producer and consumer.

* `RandStream` - produces random byte stream with optionally limited size, output ratio and fragmentation
* `Consumer` - mimics slow [io.Writer](https://golang.org/pkg/io/#Writer)
* `Shaper` - slows down and fragments/defragments your [io.Reader](https://golang.org/pkg/io/#Reader)

[![go report card](https://goreportcard.com/badge/gitlab.com/jonas.jasas/rwmock)](https://goreportcard.com/report/gitlab.com/jonas.jasas/rwmock)
[![pipeline status](https://gitlab.com/jonas.jasas/rwmock/badges/master/pipeline.svg)](https://gitlab.com/jonas.jasas/rwmock/commits/master)
[![coverage report](https://gitlab.com/jonas.jasas/rwmock/badges/master/coverage.svg)](https://gitlab.com/jonas.jasas/rwmock/commits/master)
[![godoc](https://godoc.org/gitlab.com/jonas.jasas/rwmock?status.svg)](http://godoc.org/gitlab.com/jonas.jasas/rwmock)


## Installation
Simple install the package to your [$GOPATH](https://github.com/golang/go/wiki/GOPATH "GOPATH") with the [go tool](https://golang.org/cmd/go/ "go command") from shell:
```bash
$ go get gitlab.com/jonas.jasas/rwmock
```
Make sure [Git is installed](https://git-scm.com/downloads) on your machine and in your system's `PATH`.


## Examples

### Slow random byte stream

`rs` generates 100 random byte stream with the packet sizes 1-30 and delay ~1-3 seconds between packets.
Each packet is printed to `STDOUT`.    

```go
rs := rwmock.NewRandStream(100, 1, 30, time.Second, time.Second*3)

for b, n, err := make([]byte, 50), 0, error(nil); err == nil; {
    n, err = rs.Read(b)
    log.Print(b[:n])
}
```


### Slow stream consumer

`cons` consumes fast random byte stream with the 1-3 seconds delay between reads.

```go
cons := rwmock.NewConsumer(time.Second, time.Second*3)

r := rand.New(rand.NewSource(666))
lr := &io.LimitedReader{r, 100000}

io.Copy(cons, lr)
```


### Mock slow network connection

Slowing down HTTP response body read with the 1ms-1s delays between reads.
Fragmenting reads to 1-50 bytes and printing to `STDOUT`.

```go
res, _ := http.DefaultClient.Get("http://example.com/")
sr := rwmock.NewShaperRand(res.Body, 1, 50, time.Millisecond, time.Second)

for b, n, err := make([]byte, 50), 0, error(nil); err == nil; {
    n, err = sr.Read(b)
    fmt.Print(string(b[:n]))
}
```