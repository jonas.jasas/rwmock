package rwmock

import (
	"io"
	"math/rand"
	"time"
)

// Shaper limits io.Reader throughput and fragments/defragments packets.
// Throughput rate is controlled by delaying Read() operations.
type Shaper struct {
	r         io.Reader
	minPacket int
	maxPacket int
	minDelay  int
	maxDelay  int
	lastRed   time.Time
}

// NewShaper creates shaper object.
// r - Reader that will be used as data reading source
// pckSize - packet size
// delay - delay duration between reads
// NewShaper panics if minPacket < 1
func NewShaper(r io.Reader, pckSize int, delay time.Duration) *Shaper {
	if pckSize < 1 {
		panic("minPacket must be > 0")
	}

	return &Shaper{
		r:         r,
		minPacket: pckSize,
		maxPacket: pckSize,
		minDelay:  int(delay),
		maxDelay:  int(delay),
	}
}

// NewShaperRand creates new shaper object. With the random packet size and delay.
//
// r - Reader that will be used as data reading source
//
// minPacket and maxPacket sets range of random values that will be used for delaying reads.
// minPacket - minimum packet size (may not be satisfied for the last packet)
// maxPacket - maximum packet size
//
// minDelay and maxDelay sets range of random values that will be used for delaying reads.
// minDelay - minimum delay between packets (accuracy is ~1ms)
// maxDelay - maximum delay between packets (accuracy is ~1ms)
//
// NewShaperRand panics if minPacket < 1 or minPacket > maxPacket or minDelay > maxDelay
func NewShaperRand(r io.Reader, minPacket, maxPacket int, minDelay, maxDelay time.Duration) *Shaper {
	if minPacket < 1 {
		panic("minPacket must be > 0")
	}
	if minPacket > maxPacket {
		panic("minPacket must be <= maxPacket")
	}
	if minDelay > maxDelay {
		panic("minDelay must be <= maxDelay")
	}

	return &Shaper{
		r:         r,
		minPacket: minPacket,
		maxPacket: maxPacket,
		minDelay:  int(minDelay),
		maxDelay:  int(maxDelay),
	}
}

// Read is delaying reads and fragmenting/defragmenting data packets.
func (s *Shaper) Read(b []byte) (n int, err error) {
	pckSize := s.maxPacket
	if s.minPacket < s.maxPacket {
		pckSize = rand.Intn(s.maxPacket-s.minPacket) + s.minPacket
	}

	if pckSize > len(b) {
		pckSize = len(b)
	}
	if n, err = fillBuff(s.r, b[:pckSize]); err == nil {
		delay := int64(s.maxDelay) - time.Since(s.lastRed).Nanoseconds()
		if s.minDelay < s.maxDelay {
			delay = rand.Int63n(int64(s.maxDelay-s.minDelay)) + int64(s.minDelay) - time.Since(s.lastRed).Nanoseconds()
		}
		if delay > 0 {
			delay -= int64(time.Microsecond * 100)
		} // Naive time.Sleep overhead compensation
		time.Sleep(time.Duration(delay))
		s.lastRed = time.Now()
	}
	return
}

func fillBuff(r io.Reader, b []byte) (n int, err error) {
	var rn int
	for {
		rn, err = r.Read(b[n:])
		n += rn
		if err != nil || n == len(b) {
			break
		}
	}

	return
}
